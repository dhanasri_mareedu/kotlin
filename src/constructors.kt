class Person constructor(var name:String,var age:Int) {//primary constructor
    var profession: String = "motivational tutor"
    var mobile:String="998866774455"


    constructor( name:String,age:Int,gender:String):this(name,age){//secondary constructor
        this.profession=profession
        this.mobile=mobile
    }
    fun Details(){
        println("$name whose profession is $profession is $age years old provided with mobile number $mobile  ")
    }
}
fun main(args:Array<String>){
    val person=Person("dhana",25)
    person.Details()
}