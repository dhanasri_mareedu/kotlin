fun main(args:Array<String>){

    val numbers= listOf<Int>(2,5,7,8,9,3,67,89) //immutable list and READonly
    list.add(20)   //no add(readonly)
    list.add(39)    //no add(readonly)
    list[3]=10
    list.remove(5)  //doest remove(readonly)
    list.get(6)             //No operation performed
    for(ele in 0..list.size-1){
        println(numbers[ele])  //2,5,7,8,9,3,67,89
    }

    val addNum=numbers.map({num->num+num}) //map function and {it + it)
    println(addNum) //[4,10,14,16,18,6,134,178]

    val check=numbers.filter({it<10}) //filter function
    println(check)  //[2,5,7,8,9,3]

    val squareNum=numbers.filter({num->num>10}).map({it*it}) //both map and filter
    println(squareNum)  //[4489,7921]
}