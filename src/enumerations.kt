
fun main(args:Array<String>) {
    val direction1: Direction = Direction("NORTH", DirectionColor.RED)
    val direction2: Direction = Direction("SOUTH", DirectionColor.GREEN)
    println("The color of  " + direction1.name + " is " + direction1.color) //the color of NORTH is RED
    println("The color of  " + direction2.name + " is " + direction2.color) //the color of SOUTH is GREEN
    println(direction1.color.toString() + " value is " + direction1.color.rgb ) //RED value is 16711680
    println(direction2.color.toString() + " value is " + direction2.color.rgb) //GREEN value is 65280
}
data class Direction(val name:String,val color:DirectionColor)

enum class DirectionColor(val rgb: Int) {
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF),
}
