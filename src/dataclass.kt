
data class Movie(var name: String, var studio: String, var rating: Float)

val movie = Movie("The Dark Princess", "Sony Pictures", 8.5F)
fun main(args:Array<String>) {
    println(movie.name)   //The Dark princess
    println(movie.studio) //Sony Pictures
    println(movie.rating) //8.5
    movie.rating = 9F
    println(movie.toString()) // Movie(name=The Dark Princess, studio=Sony Pictures, rating=9.0)
}


//val betterRating = movie.copy(rating = 9.5F)//Movie(name=The Dark Princess, studio=Sony Pictures, rating=9.5)
//val animatedMovie=movie.copy(name="The House of Baskerville")//Movie(name=The House of Baskerville, studio=Sony Pictures, rating=8.5)
//val Studio=movie.copy(studio="Anapurna Studio")//Movie(name=The Dark Princess, studio=Anapurna Studio, rating=8.5)
//fun main(args:Array<String>) {//copy function
//    println(betterRating.toString())
//    println(Studio.toString())
//    println(animatedMovie.toString())}

//destructuring declarations
//    movie.component1() // name
//    movie.component2() // studio
//    movie.component3() // rating


//creating multiple variables from the object
    //val (name, studio, rating) = movie

   // fun getMovieInfo() = movie
    //val (namef, studiof, ratingf) = getMovieInfo()
//}
//}