class Outer1 {
    public val bar: Int = 1
    inner class Inner {
        fun foo() = bar
    }
}
fun main(args: Array<String>) {
    println(Outer1().Inner().foo())

}