var arr1= intArrayOf(5)          //array of int of size 5
var arr2:IntArray= intArrayOf(5) //type int array of size 5
var numbers:IntArray= intArrayOf(2,3,4,6,7,66)//type of array int with elements
var numbers1= floatArrayOf(45f,4f,67.0f)//array of float with float declaration
var numbers2= intArrayOf(2,66,88,45,89)//int array with elements

var numbers4:IntArray= IntRange(50,20).step(10).toList().toIntArray()//array declaration with range.
var numbers3=IntRange(100,20).step(5).toList().toString().toBigDecimal().toFloat().toInt()


myArray[3]//points to the index3 and gives value in index3
myArray.get(3)//gets the value of 3
myArray[3]=20//sets the value in 3rd index  to 20.
myArray.set(3,20)// sets the value to 20 in the 3rd index.

   fun main(args:Array<String>){
   var arr1:IntArray= IntRange(10,100).step(10).toList().toIntArray()
    for (num in arr1)
        println(num)//prints a list of numbers 10 20 30 40 50 60 70 80 90 100
   }

   val asc = Array(5, { i -> (i * i).toString() })
   fun main(args:Array<String>){
    println(asc[4])//display value 16.
   }



