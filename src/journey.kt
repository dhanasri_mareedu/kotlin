data class Journey(var start:String,var destination:String,var time:Float,var reservation:String)//data class declaration
val journey=Journey("vijayawada" ,"hyderabad",09.00F,"ODE8876")//object creation to Journey
//fun main(args:Array<String>){  //main function declaration
//    println(journey.start)
//    println(journey.destination)
//    println(journey.time)
//    println(journey.toString())// printing all the components in the declaration
//}

var happyJourney=journey.copy(reservation="BSDJF68757")//adding a factor to the reservation
fun main(args:Array<String>){
    println(happyJourney.toString())//full component declaration with the change of "reservation component"
}


