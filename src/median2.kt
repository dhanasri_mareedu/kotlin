
fun median2(l: List<Int>) = l.sorted().let { (it[it.size / 2] + it[(it.size - 1) / 2]) / 2.0F }

fun main(args: Array<String>) {
    median2(listOf(12,4,5,3,8,7)).let { println(it) }

}


