sealed class Colour{
    class Red(val bright:String):Colour()
    class Blue(val bright:String):Colour()
     class Orange(val bright:String):Colour()
    class Green(val bright:String):Colour()
}
fun main(args:Array<String>){
    val colour:Colour=Colour.Blue(bright="pleasent")
    val output=when(colour){
        is Colour.Red->"red"+colour.bright
        is Colour.Blue->"blue"+colour.bright
        is Colour.Orange->"Orange"+colour.bright
        is Colour.Green->"Green"+colour.bright
    }
}