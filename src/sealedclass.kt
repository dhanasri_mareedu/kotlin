 sealed class Operation {
    class Add(val value: Int) : Operation()
    class Substract(val value: Int) : Operation()
    class Multiplication(val value:Int):Operation()
     class  Division(val  value:Int):Operation()

}
fun main(args:Array<String>){
    val operation:Operation=Operation.Division(value = 100)
    val output = when (operation){
        is Operation.Add -> 4+operation.value
        is Operation.Substract -> 8-operation.value
        is Operation.Multiplication->10*operation.value
        is Operation.Division->2000/operation.value

    }
    println(output); //20
}

// sealed class Numbers{
//     class One(val value:Int):Number()
//     class Two(val value:Int):Number()
//     class Three(val value:Int):Number()
// }
// fun main(args:Array<String>){
//     val number=Number=Number.Three(value=1)
//     val output=when(Number){
//         is Number.one->
//     }
// }