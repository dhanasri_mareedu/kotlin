fun main(args:Array<String>)
{
    var myNumbers= listOf<Int>(1,4,6,8,45,67,78)

    val predicate={num:Int->num>10}//instead of {it>10} or lambda expression also

    // does all element satisfy the predicate
    val all: Boolean =myNumbers.all ({ num->num>10})
    println(all)//false

    //does any element satisfy the predicate
    val any:Boolean=myNumbers.any({it>10})
    println(any)//true

    //returns the count that satisfy the predicate
    val count:Int=myNumbers.count({it>10})
    println(count)//3

    //returns the first element that satisfy predicate
     val find=myNumbers.find(predicate)
    println(find)//45
}
