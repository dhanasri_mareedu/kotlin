fun median(l: List<Int>) = l.sorted().let { (it[it.size / 2] + it[(it.size - 1) / 2]) / 2 }

fun main(args: Array<String>) {
    median(listOf(12, 4, 5, 3, 8, 7, 6, 1, 0)).let {
    }
    median(listOf(12, 4)).let {
        listOf(12,4).sorted()
        val it2: Float = it.toFloat()
        println("the median of{12,4} is $it2")
    }
    median(listOf(12, 4, 5)).let {
        listOf(12,4,5).sorted()
        val it2: Float = it.toFloat()
        println("the median of{12,4,5} is $it2")
    }
    median(listOf(12, 4, 5, 3)).let {
        listOf(12,4,5,3).sorted()
        val it2: Float = it.toFloat()
        println("the median of{12,4,5,3} is $it2")
    }
    median(listOf(12, 4, 5, 3, 8)).let {
        listOf(12,4,5,3,8).sorted()
        //println(it)
        val it2: Float = it.toFloat()
        println("the median of{12,4,5,3,8} is $it2")
    }
    median(listOf(12, 4, 5, 3, 8, 7)).let {
        listOf(12,4,5,3,8,7).sorted()
       // println(it)
        val it2: Float = it.toFloat()
        println("the median of{12,4,5,3,8,7} is $it2")
    }
    median(listOf(12, 4, 5, 3, 8, 7, 6)).let {
        listOf(12,4,5,3,8,7,6).sorted()
        //println(it)
        val it2: Float = it.toFloat()
        println("the median of{12,4,5,3,8,7,6} is $it2")
    }
    median(listOf(12, 4, 5, 3, 8, 7, 6, 1)).let {
        listOf(12,4,5,3,8,7,6,1).sorted()
        //println(it)
        val it2: Float = it.toFloat()
        println("the median of{12,4,5,3,8,7,6,1} is $it2")
    }
    median(listOf(12, 4, 5, 3, 8, 7, 6, 1, 0)).let {
        listOf(12,4,5,3,8,7,6,1,0).sorted()
        //println(it)
        val it2: Float = it.toFloat()
        println("the median of{12,4,5,3,8,7,6,1,0} is $it2")
    }
}
